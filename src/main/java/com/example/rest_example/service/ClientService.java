package com.example.rest_example.service;

import com.example.rest_example.bean.Client;

import java.util.List;

public interface ClientService {

    void create(Client client);

    List<Client> getAll();

    Client getById(long id);

    boolean update(Client client, long id);

    boolean delete(long id);
}
